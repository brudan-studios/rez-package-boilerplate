name = "boilerplate"

version = "0.0.1"

authors = [
    "Heorhi Samushyia"
]

description = \
    """
    Rez package boilerplate.
    """

tools = [
]

build_command = 'python -m rezutil build {root}'
private_build_requires = ['rezutil']


def commands():
    pass